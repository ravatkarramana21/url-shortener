package main

import (
    "errors" 
    "encoding/json"
    "log"
    "net/http"
    "strings"
    "sync"
)


// URLService handles URL shortening, redirection, and metrics
type URLService struct {
	urlStore     map[string]string // shortenedID -> originalURL
	metricsStore map[string]int    // domain -> count
	mu           sync.Mutex        // mutex for thread safety
}

func NewURLService() *URLService {
	return &URLService{
		urlStore:     make(map[string]string),
		metricsStore: make(map[string]int),
	}
}

func (s *URLService) ShortenURL(url string) (string, error) {
	// Check if the URL is already shortened
	for short, original := range s.urlStore {
		if original == url {
			return short, nil
		}
	}

	// Generate shortened ID (simplified for example)
	shortenedID := "abc123" // Replace with actual ID generation logic
	s.urlStore[shortenedID] = url

	// Update metrics
	s.UpdateMetrics(url)

	return shortenedID, nil
}

func (s *URLService) Redirect(shortenedID string) (string, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	originalURL, ok := s.urlStore[shortenedID]
	if !ok {
		return "", errors.New("shortened URL not found")
	}
	return originalURL, nil
}

func (s *URLService) UpdateMetrics(originalURL string) {
	// Extract domain from originalURL
	parts := strings.Split(originalURL, "/")
	if len(parts) < 3 {
		return
	}
	domain := parts[2]

	// Update metrics
	s.metricsStore[domain]++
}

func (s *URLService) Top3Domains() map[string]int {
	// Sort metricsStore and return top 3
	// Simplified logic for top 3, adjust as needed
	top3 := make(map[string]int)
	for domain, count := range s.metricsStore {
		if len(top3) < 3 {
			top3[domain] = count
		}
	}
	return top3
}

// Handlers
func shortenHandler(w http.ResponseWriter, r *http.Request) {
	var payload struct {
		URL string `json:"url"`
	}
	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
		http.Error(w, "invalid request payload", http.StatusBadRequest)
		return
	}

	shortenedID, err := urlService.ShortenURL(payload.URL)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response := map[string]string{"shortened_url": shortenedID}
	jsonResponse(w, response)
}

func redirectHandler(w http.ResponseWriter, r *http.Request) {
	shortenedID := r.URL.Path[len("/"):]

	originalURL, err := urlService.Redirect(shortenedID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	http.Redirect(w, r, originalURL, http.StatusFound)
}

func top3DomainsHandler(w http.ResponseWriter, r *http.Request) {
	top3 := urlService.Top3Domains()
	jsonResponse(w, top3)
}

func jsonResponse(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

var urlService *URLService

func main() {
	urlService = NewURLService()

	http.HandleFunc("/shorten", shortenHandler)
	http.HandleFunc("/", redirectHandler)
	http.HandleFunc("/metrics/top3", top3DomainsHandler)
	
	log.Fatal(http.ListenAndServe(":8081", nil))

}
